<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="yes"/>
<layer number="100" name="PaJa" color="12" fill="7" visible="no" active="yes"/>
<layer number="101" name="Doplnky" color="5" fill="1" visible="no" active="yes"/>
<layer number="102" name="Kola" color="2" fill="1" visible="yes" active="yes"/>
<layer number="103" name="Popisy" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Zapojeni" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="pin_numbers" color="12" fill="1" visible="yes" active="yes"/>
<layer number="106" name="delate" color="0" fill="1" visible="yes" active="yes"/>
<layer number="107" name="pic" color="9" fill="1" visible="yes" active="yes"/>
<layer number="108" name="poznamky" color="15" fill="1" visible="yes" active="yes"/>
<layer number="109" name="smd" color="11" fill="1" visible="yes" active="yes"/>
<layer number="110" name="wago-seda" color="7" fill="8" visible="yes" active="yes"/>
<layer number="111" name="wago-cervena" color="12" fill="8" visible="yes" active="yes"/>
<layer number="112" name="wago-zelena" color="2" fill="8" visible="yes" active="yes"/>
<layer number="113" name="wago-modra" color="1" fill="8" visible="yes" active="yes"/>
<layer number="114" name="out" color="14" fill="1" visible="yes" active="yes"/>
<layer number="115" name="Text" color="15" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="no"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="yes" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="yes" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="yes" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="no" active="no"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="#hahahalib">
<packages>
<package name="DISCO-F746NG">
<pad name="NC" x="0" y="40.64" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="IOREF" x="0" y="38.1" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="NRST" x="0" y="35.56" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="3V3" x="0" y="33.02" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="5V" x="0" y="30.48" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="GND" x="0" y="27.94" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="GND2" x="0" y="25.4" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="VIN" x="0" y="22.86" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PF7" x="0" y="7.62" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PF8" x="0" y="10.16" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PF9" x="0" y="12.7" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PF10" x="0" y="15.24" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PA0" x="0" y="17.78" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PF6" x="0" y="5.08" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PC7" x="48.26" y="5.08" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PC6" x="48.26" y="7.62" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PG6" x="48.26" y="10.16" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PB4" x="48.26" y="12.7" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PG7" x="48.26" y="15.24" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PI0" x="48.26" y="17.78" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PH6" x="48.26" y="20.32" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PI3" x="48.26" y="22.86" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PI2" x="48.26" y="27.94" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PA15" x="48.26" y="30.48" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PA8" x="48.26" y="33.02" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PB15" x="48.26" y="35.56" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PB14" x="48.26" y="38.1" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PI1" x="48.26" y="40.64" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="GND3" x="48.26" y="43.18" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="AVDD" x="48.26" y="45.72" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PB9" x="48.26" y="48.26" drill="0.8" diameter="1.6764" shape="long"/>
<pad name="PB8" x="48.26" y="50.8" drill="0.8" diameter="1.6764" shape="long"/>
<wire x1="-2.54" y1="53.34" x2="-2.54" y2="2.54" width="0.127" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="50.8" y2="2.54" width="0.127" layer="21"/>
<wire x1="50.8" y1="2.54" x2="50.8" y2="53.34" width="0.127" layer="21"/>
<wire x1="50.8" y1="53.34" x2="-2.54" y2="53.34" width="0.127" layer="21"/>
</package>
<package name="BLUETOOTH-HC-05">
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-10.16" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-10.16" x2="2.54" y2="-10.16" width="0.127" layer="21"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="7.62" width="0.127" layer="21"/>
<wire x1="2.54" y1="7.62" x2="-2.54" y2="7.62" width="0.127" layer="21"/>
<pad name="1" x="0" y="5.08" drill="0.8" diameter="1.016" shape="long"/>
<pad name="2" x="0" y="2.54" drill="0.8" diameter="1.016" shape="long"/>
<pad name="3" x="0" y="0" drill="0.8" diameter="1.016" shape="long"/>
<pad name="4" x="0" y="-2.54" drill="0.8" diameter="1.016" shape="long"/>
<pad name="5" x="0" y="-5.08" drill="0.8" diameter="1.016" shape="long"/>
<pad name="6" x="0" y="-7.62" drill="0.8" diameter="1.016" shape="long"/>
</package>
</packages>
<symbols>
<symbol name="DISCO-F746NG">
<wire x1="-35.56" y1="-22.86" x2="-35.56" y2="27.94" width="0.254" layer="94"/>
<wire x1="-35.56" y1="27.94" x2="38.1" y2="27.94" width="0.254" layer="94"/>
<wire x1="38.1" y1="27.94" x2="38.1" y2="-22.86" width="0.254" layer="94"/>
<wire x1="38.1" y1="-22.86" x2="-35.56" y2="-22.86" width="0.254" layer="94"/>
<pin name="IOREF" x="-40.64" y="12.7" length="middle"/>
<pin name="NRST" x="-40.64" y="10.16" length="middle"/>
<pin name="+3V3" x="-40.64" y="7.62" length="middle" direction="pwr"/>
<pin name="+5V" x="-40.64" y="5.08" length="middle" direction="pwr"/>
<pin name="GND@1" x="-40.64" y="2.54" length="middle" direction="pwr"/>
<pin name="GND@2" x="-40.64" y="0" length="middle" direction="pwr" swaplevel="1"/>
<pin name="VIN" x="-40.64" y="-2.54" length="middle" direction="pwr"/>
<pin name="PA_0/A0/PWM2/SERIAL4_TX" x="-40.64" y="-7.62" length="middle"/>
<pin name="PF_10/A1" x="-40.64" y="-10.16" length="middle"/>
<pin name="PF_9/A2/PWM14/SPI5MOSI" x="-40.64" y="-12.7" length="middle"/>
<pin name="PF_8/A3/PWM13/SPI5MISO" x="-40.64" y="-15.24" length="middle"/>
<pin name="PF_7/A4/PWM_11/SPI5SCK/SERIAL7TX" x="-40.64" y="-17.78" length="middle"/>
<pin name="PF_6/A5/PWM10/SPI5NSS/SERIAL7RX" x="-40.64" y="-20.32" length="middle"/>
<pin name="PC_7/D0/SERIAL6RX/PWM8" x="43.18" y="-20.32" length="middle" rot="R180"/>
<pin name="PC_6/D1/SERIAL6TX/PWM8" x="43.18" y="-17.78" length="middle" rot="R180"/>
<pin name="PG_6/D2" x="43.18" y="-15.24" length="middle" rot="R180"/>
<pin name="PB_4/D3/SPI2NSS/PWM3" x="43.18" y="-12.7" length="middle" rot="R180"/>
<pin name="PG_7/D4" x="43.18" y="-10.16" length="middle" rot="R180"/>
<pin name="PI_0/D5/SPI2NSS" x="43.18" y="-7.62" length="middle" rot="R180"/>
<pin name="PH_6/D6/SPI5SCK/PWM12" x="43.18" y="-5.08" length="middle" rot="R180"/>
<pin name="PI_3/D7/SPI2MOSI" x="43.18" y="-2.54" length="middle" rot="R180"/>
<pin name="PI_2/D8/SPI2MISO/PWM8" x="43.18" y="2.54" length="middle" rot="R180"/>
<pin name="PA_15/D9/PWM2" x="43.18" y="5.08" length="middle" rot="R180"/>
<pin name="PA_8/D10/PWM1" x="43.18" y="7.62" length="middle" rot="R180"/>
<pin name="PB_15/D11/SPI2MOSI/PWM12" x="43.18" y="10.16" length="middle" rot="R180"/>
<pin name="PB_14/D12/SPI2MISO/PWM12" x="43.18" y="12.7" length="middle" rot="R180"/>
<pin name="PI_1/D13/SPI2SCK/LED1" x="43.18" y="15.24" length="middle" rot="R180"/>
<pin name="GND@3" x="43.18" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="AVDD" x="43.18" y="20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="PB_9/D14/I2C1SDA/PWM4/SPI2NSS" x="43.18" y="22.86" length="middle" rot="R180"/>
<pin name="PB_8/D15/I2C1SCL/PWM4" x="43.18" y="25.4" length="middle" rot="R180"/>
<text x="-35.56" y="27.94" size="1.778" layer="96">&gt;VALUE</text>
<text x="-35.56" y="-25.4" size="1.778" layer="95">&gt;NAME</text>
<pin name="NC" x="-40.64" y="15.24" length="middle"/>
</symbol>
<symbol name="BLUETOOTH-HC-05">
<wire x1="-5.08" y1="12.7" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="12.7" width="0.254" layer="94"/>
<wire x1="5.08" y1="12.7" x2="-5.08" y2="12.7" width="0.254" layer="94"/>
<pin name="STATE" x="-10.16" y="10.16" length="middle"/>
<pin name="RX" x="-10.16" y="7.62" length="middle"/>
<pin name="TX" x="-10.16" y="5.08" length="middle"/>
<pin name="GND" x="-10.16" y="2.54" length="middle"/>
<pin name="5V" x="-10.16" y="0" length="middle"/>
<pin name="EN" x="-10.16" y="-2.54" length="middle"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<text x="-5.08" y="12.7" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-7.62" size="1.778" layer="95">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DISCO+F746NG" prefix="IC" uservalue="yes">
<gates>
<gate name="G$1" symbol="DISCO-F746NG" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DISCO-F746NG">
<connects>
<connect gate="G$1" pin="+3V3" pad="3V3"/>
<connect gate="G$1" pin="+5V" pad="5V"/>
<connect gate="G$1" pin="AVDD" pad="AVDD"/>
<connect gate="G$1" pin="GND@1" pad="GND"/>
<connect gate="G$1" pin="GND@2" pad="GND2"/>
<connect gate="G$1" pin="GND@3" pad="GND3"/>
<connect gate="G$1" pin="IOREF" pad="IOREF"/>
<connect gate="G$1" pin="NC" pad="NC"/>
<connect gate="G$1" pin="NRST" pad="NRST"/>
<connect gate="G$1" pin="PA_0/A0/PWM2/SERIAL4_TX" pad="PA0"/>
<connect gate="G$1" pin="PA_15/D9/PWM2" pad="PA15"/>
<connect gate="G$1" pin="PA_8/D10/PWM1" pad="PA8"/>
<connect gate="G$1" pin="PB_14/D12/SPI2MISO/PWM12" pad="PB14"/>
<connect gate="G$1" pin="PB_15/D11/SPI2MOSI/PWM12" pad="PB15"/>
<connect gate="G$1" pin="PB_4/D3/SPI2NSS/PWM3" pad="PB4"/>
<connect gate="G$1" pin="PB_8/D15/I2C1SCL/PWM4" pad="PB8"/>
<connect gate="G$1" pin="PB_9/D14/I2C1SDA/PWM4/SPI2NSS" pad="PB9"/>
<connect gate="G$1" pin="PC_6/D1/SERIAL6TX/PWM8" pad="PC6"/>
<connect gate="G$1" pin="PC_7/D0/SERIAL6RX/PWM8" pad="PC7"/>
<connect gate="G$1" pin="PF_10/A1" pad="PF10"/>
<connect gate="G$1" pin="PF_6/A5/PWM10/SPI5NSS/SERIAL7RX" pad="PF6"/>
<connect gate="G$1" pin="PF_7/A4/PWM_11/SPI5SCK/SERIAL7TX" pad="PF7"/>
<connect gate="G$1" pin="PF_8/A3/PWM13/SPI5MISO" pad="PF8"/>
<connect gate="G$1" pin="PF_9/A2/PWM14/SPI5MOSI" pad="PF9"/>
<connect gate="G$1" pin="PG_6/D2" pad="PG6"/>
<connect gate="G$1" pin="PG_7/D4" pad="PG7"/>
<connect gate="G$1" pin="PH_6/D6/SPI5SCK/PWM12" pad="PH6"/>
<connect gate="G$1" pin="PI_0/D5/SPI2NSS" pad="PI0"/>
<connect gate="G$1" pin="PI_1/D13/SPI2SCK/LED1" pad="PI1"/>
<connect gate="G$1" pin="PI_2/D8/SPI2MISO/PWM8" pad="PI2"/>
<connect gate="G$1" pin="PI_3/D7/SPI2MOSI" pad="PI3"/>
<connect gate="G$1" pin="VIN" pad="VIN"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="BLUETOOTH-HC-05" prefix="MOD">
<gates>
<gate name="G$1" symbol="BLUETOOTH-HC-05" x="-7.62" y="7.62"/>
</gates>
<devices>
<device name="" package="BLUETOOTH-HC-05">
<connects>
<connect gate="G$1" pin="5V" pad="5"/>
<connect gate="G$1" pin="EN" pad="6"/>
<connect gate="G$1" pin="GND" pad="4"/>
<connect gate="G$1" pin="RX" pad="2"/>
<connect gate="G$1" pin="STATE" pad="1"/>
<connect gate="G$1" pin="TX" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#PaJa_konektory">
<description>&lt;B&gt;PaJa_konektory&lt;/B&gt; - knihovna   &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 
&lt;I&gt;(vytvoreno 1.6.2011)&lt;/I&gt;&lt;BR&gt;
Knihovna konektoru do Eagle &lt;I&gt;(od verze 5.6)&lt;/I&gt;&lt;BR&gt;
&lt;BR&gt;
Knihovna obsahuje: 91 soucastek na DPS, 92 do SCHematu&lt;BR&gt;
&lt;BR&gt;
&lt;Author&gt;Copyright (C) PaJa 2011&lt;BR&gt;
http://www.paja-trb.unas.cz&lt;BR&gt;
paja-trb@seznam.cz
&lt;/author&gt;</description>
<packages>
<package name="ARK500/2">
<wire x1="-5.08" y1="-3.556" x2="-5.08" y2="-2.159" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.937" x2="-5.08" y2="3.937" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.937" x2="5.08" y2="3.088" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.199" x2="5.08" y2="2.159" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-3.556" x2="5.08" y2="-3.556" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.159" x2="-5.08" y2="3.937" width="0.127" layer="21"/>
<wire x1="-5.08" y1="2.159" x2="5.08" y2="2.159" width="0.127" layer="21"/>
<wire x1="5.08" y1="2.159" x2="5.08" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.159" x2="5.08" y2="-2.159" width="0.127" layer="21"/>
<wire x1="-5.08" y1="-2.159" x2="-5.08" y2="2.159" width="0.127" layer="21"/>
<wire x1="5.08" y1="-2.159" x2="5.08" y2="-3.556" width="0.127" layer="21"/>
<wire x1="1.4986" y1="-1.397" x2="3.9116" y2="1.016" width="0.127" layer="51"/>
<wire x1="1.1176" y1="-1.016" x2="3.5306" y2="1.397" width="0.127" layer="51"/>
<wire x1="-3.9116" y1="-1.016" x2="-1.4986" y2="1.397" width="0.127" layer="51"/>
<wire x1="-3.5306" y1="-1.397" x2="-1.1176" y2="1.016" width="0.127" layer="51"/>
<wire x1="1.4986" y1="-1.016" x2="3.5306" y2="1.016" width="0.6096" layer="51"/>
<wire x1="-3.5306" y1="-1.016" x2="-1.4986" y2="1.016" width="0.6096" layer="51"/>
<wire x1="1.1176" y1="-1.016" x2="1.4986" y2="-1.397" width="0.127" layer="51"/>
<wire x1="3.5306" y1="1.397" x2="3.9116" y2="1.016" width="0.127" layer="51"/>
<wire x1="-3.9116" y1="-1.016" x2="-3.5306" y2="-1.397" width="0.127" layer="51"/>
<wire x1="-1.4986" y1="1.397" x2="-1.1176" y2="1.016" width="0.127" layer="51"/>
<wire x1="5.08" y1="2.199" x2="5.588" y2="2.072" width="0.127" layer="21"/>
<wire x1="5.588" y1="2.072" x2="5.588" y2="3.215" width="0.127" layer="21"/>
<wire x1="5.588" y1="3.215" x2="5.08" y2="3.088" width="0.127" layer="21"/>
<wire x1="5.08" y1="3.088" x2="5.08" y2="2.199" width="0.127" layer="21"/>
<circle x="2.5146" y="3.048" radius="0.508" width="0.127" layer="21"/>
<circle x="-2.5146" y="3.048" radius="0.508" width="0.127" layer="21"/>
<circle x="2.5146" y="0" radius="1.778" width="0.127" layer="51"/>
<circle x="-2.5146" y="0" radius="1.778" width="0.127" layer="51"/>
<circle x="-2.54" y="0" radius="0.7099" width="0.127" layer="102"/>
<circle x="2.54" y="0" radius="0.7099" width="0.127" layer="102"/>
<pad name="1" x="-2.54" y="0" drill="1.27" diameter="3.2" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.27" diameter="3.2" shape="octagon"/>
<text x="-2.695" y="-5.398" size="1.4224" layer="25">&gt;NAME</text>
<text x="-4.7093" y="-3.33" size="1.016" layer="27">&gt;VALUE</text>
<text x="-4.127" y="2.54" size="0.9906" layer="21" ratio="12">1</text>
<text x="0.794" y="2.54" size="0.9906" layer="21" ratio="12">2</text>
<text x="-0.477" y="2.2866" size="0.254" layer="100">PaJa</text>
<text x="2.0377" y="-3.4199" size="0.6096" layer="101">dratu</text>
<text x="1.7395" y="-2.7752" size="0.6096" layer="101">strana</text>
<rectangle x1="-0.381" y1="-1.905" x2="0.381" y2="1.905" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="ARK500/2">
<wire x1="-1.268" y1="1.906" x2="-1.268" y2="-4.446" width="0.254" layer="94"/>
<wire x1="-1.268" y1="-4.446" x2="3.178" y2="-4.446" width="0.254" layer="94"/>
<wire x1="3.178" y1="-4.446" x2="3.178" y2="1.906" width="0.254" layer="94"/>
<wire x1="3.178" y1="1.906" x2="-1.268" y2="1.906" width="0.254" layer="94"/>
<wire x1="-0.317" y1="-1.268" x2="2.219" y2="1.268" width="0.127" layer="94"/>
<wire x1="-0.317" y1="-3.804" x2="2.219" y2="-1.268" width="0.127" layer="94"/>
<circle x="0.951" y="0" radius="0.951" width="0.254" layer="94"/>
<circle x="0.951" y="-2.536" radius="0.951" width="0.254" layer="94"/>
<text x="-1.8865" y="2.539" size="1.778" layer="95">&gt;Name</text>
<text x="-1.8865" y="-6.979" size="1.778" layer="96">&gt;Value</text>
<text x="1.897" y="-4.1245" size="0.254" layer="100">PaJa</text>
<pin name="K1" x="-5.08" y="0" visible="pad" length="middle" direction="pas" swaplevel="1"/>
<pin name="K2" x="-5.08" y="-2.54" visible="pad" length="middle" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ARK500/2" prefix="K">
<description>&lt;B&gt;Svorkovnice&lt;/B&gt; - roztec 5mm - dvojita</description>
<gates>
<gate name="K" symbol="ARK500/2" x="-40.64" y="35.56" swaplevel="1"/>
</gates>
<devices>
<device name="" package="ARK500/2">
<connects>
<connect gate="K" pin="K1" pad="1"/>
<connect gate="K" pin="K2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+3V3">
<wire x1="1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.54" y="-5.08" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+3V3" prefix="+3V3">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="#PaJa_30">
<description>&lt;B&gt;PaJa 30&lt;/B&gt; - knihovna   &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; &amp;nbsp; 
&lt;I&gt;(vytvoreno 1.6.2011)&lt;/I&gt;&lt;BR&gt;
Univerzální knihovna soucastek do Eagle &lt;I&gt;(od verze 5.6)&lt;/I&gt;&lt;BR&gt;
&lt;BR&gt;
Knihovna obsahuje: 196 soucastek na DPS, 298 do SCHematu&lt;BR&gt;
&lt;BR&gt;
&lt;Author&gt;Copyright (C) PaJa 2001-2011&lt;BR&gt;
http://www.paja-trb.unas.cz&lt;BR&gt;
paja-trb@seznam.cz
&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="+12V">
<wire x1="-1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.5399" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="0.9525" y="1.905" size="1.778" layer="96" rot="R90">&gt;Value</text>
<text x="1.27" y="-5.3975" size="1.016" layer="101" ratio="6" rot="R90">+12V</text>
<text x="-0.1588" y="-1.905" size="0.254" layer="100" rot="R90">PaJa</text>
<pin name="+12V" x="0" y="-5.08" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="GND">
<wire x1="-1.7463" y1="-0.0001" x2="1.7463" y2="-0.0001" width="0.6096" layer="94"/>
<text x="-1.1113" y="0.3175" size="0.254" layer="100">PaJa</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+5V">
<wire x1="-1.27" y1="-1.905" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.5399" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="0.9525" y="0.635" size="1.778" layer="96" rot="R90">&gt;Value</text>
<text x="1.27" y="-4.445" size="1.016" layer="101" ratio="6" rot="R90">+5V</text>
<pin name="+5V" x="0" y="-5.08" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="+12V" prefix="NAP">
<description>&lt;B&gt;SCH symbol&lt;/B&gt; - napajeni +12V</description>
<gates>
<gate name="+12" symbol="+12V" x="0" y="-5.08"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;B&gt;SCH symbol&lt;/B&gt; - zem - &lt;I&gt;GrouND&lt;/I&gt;</description>
<gates>
<gate name="ZEM" symbol="GND" x="-45.72" y="35.56"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="NAP">
<description>&lt;B&gt;SCH symbol&lt;/B&gt; - napajeni +5V</description>
<gates>
<gate name="+5" symbol="+5V" x="0" y="-5.08"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="frames">
<description>&lt;b&gt;Frames for Sheet and Layout&lt;/b&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="A4L-LOC">
<wire x1="256.54" y1="3.81" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="256.54" y1="8.89" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="256.54" y1="13.97" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="256.54" y1="19.05" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="3.81" x2="161.29" y2="24.13" width="0.1016" layer="94"/>
<wire x1="161.29" y1="24.13" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<wire x1="215.265" y1="24.13" x2="256.54" y2="24.13" width="0.1016" layer="94"/>
<wire x1="246.38" y1="3.81" x2="246.38" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="256.54" y2="8.89" width="0.1016" layer="94"/>
<wire x1="246.38" y1="8.89" x2="215.265" y2="8.89" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="3.81" width="0.1016" layer="94"/>
<wire x1="215.265" y1="8.89" x2="215.265" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="256.54" y2="13.97" width="0.1016" layer="94"/>
<wire x1="215.265" y1="13.97" x2="215.265" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="256.54" y2="19.05" width="0.1016" layer="94"/>
<wire x1="215.265" y1="19.05" x2="215.265" y2="24.13" width="0.1016" layer="94"/>
<text x="217.17" y="15.24" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="217.17" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="230.505" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="216.916" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<frame x1="0" y1="0" x2="260.35" y2="179.07" columns="6" rows="4" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="A4L-LOC" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with location and doc. field</description>
<gates>
<gate name="G$1" symbol="A4L-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="IC1" library="#hahahalib" deviceset="DISCO+F746NG" device="" value="STM32F746NG-DISCO"/>
<part name="NAP3" library="#PaJa_30" deviceset="+12V" device=""/>
<part name="GND4" library="#PaJa_30" deviceset="GND" device=""/>
<part name="K2" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K10" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K12" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K13" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K14" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K4" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K5" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K6" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="MAINBOARD" library="frames" deviceset="A4L-LOC" device="" value="řídící deska tří osého robotického manipulátoru"/>
<part name="GND1" library="#PaJa_30" deviceset="GND" device=""/>
<part name="NAP1" library="#PaJa_30" deviceset="+12V" device=""/>
<part name="MOD5" library="#hahahalib" deviceset="BLUETOOTH-HC-05" device="" value="BLUETOOTH-HC-05-MODULE"/>
<part name="GND8" library="#PaJa_30" deviceset="GND" device=""/>
<part name="NAP8" library="#PaJa_30" deviceset="+5V" device=""/>
<part name="K8" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K15" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="K16" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
<part name="NAP5" library="#PaJa_30" deviceset="+5V" device=""/>
<part name="NAP7" library="#PaJa_30" deviceset="+12V" device=""/>
<part name="GND2" library="#PaJa_30" deviceset="GND" device=""/>
<part name="+3V1" library="supply1" deviceset="+3V3" device=""/>
<part name="K1" library="#PaJa_konektory" deviceset="ARK500/2" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<text x="-31.75" y="39.37" size="1.778" layer="97">12V</text>
<text x="-31.75" y="36.83" size="1.778" layer="97">GND</text>
<text x="10.16" y="-45.72" size="1.778" layer="97">4x motorový driver X-NUCLEO-IHM03A1</text>
<text x="49.53" y="-96.52" size="3.81" layer="94" font="vector">Hlavní deska
robotické ruky</text>
<text x="100.33" y="-86.36" size="2.54" layer="94" font="vector">Martina Hanusová</text>
<text x="130.81" y="-101.6" size="2.54" layer="94" font="vector">V1.0</text>
<text x="-22.86" y="48.26" size="1.778" layer="115">Napájení motorových driverů</text>
<text x="-25.4" y="-48.26" size="1.778" layer="115">Bluetooth modul HC-05</text>
<text x="111.76" y="12.7" size="1.778" layer="115">I2C senzory</text>
<text x="43.18" y="-33.02" size="1.778" layer="95">CS4</text>
<text x="-78.74" y="-48.26" size="1.778" layer="91" align="bottom-center"></text>
<text x="106.68" y="-30.48" size="1.778" layer="95">Servomotory a senzory
druhé robotické ruky</text>
</plain>
<instances>
<instance part="IC1" gate="G$1" x="-10.16" y="-12.7"/>
<instance part="NAP3" gate="+12" x="17.78" y="45.72"/>
<instance part="GND4" gate="ZEM" x="-58.42" y="-12.7"/>
<instance part="K2" gate="K" x="0" y="40.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="1.8865" y="43.179" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="1.8865" y="31.121" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="K10" gate="K" x="101.6" y="0" smashed="yes">
<attribute name="NAME" x="99.7135" y="2.539" size="1.778" layer="95"/>
</instance>
<instance part="K12" gate="K" x="101.6" y="-15.24" smashed="yes">
<attribute name="NAME" x="99.7135" y="-12.701" size="1.778" layer="95"/>
</instance>
<instance part="K13" gate="K" x="101.6" y="-45.72" smashed="yes">
<attribute name="NAME" x="99.7135" y="-43.181" size="1.778" layer="95"/>
</instance>
<instance part="K14" gate="K" x="101.6" y="-25.4" smashed="yes">
<attribute name="NAME" x="99.7135" y="-22.861" size="1.778" layer="95"/>
</instance>
<instance part="K4" gate="K" x="-15.24" y="40.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="-13.3535" y="43.179" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-13.3535" y="31.121" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="K5" gate="K" x="-22.86" y="40.64" rot="MR0"/>
<instance part="K6" gate="K" x="-7.62" y="40.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="-5.7335" y="43.179" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="-5.7335" y="33.661" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="MAINBOARD" gate="G$1" x="-116.84" y="-106.68"/>
<instance part="GND1" gate="ZEM" x="17.78" y="35.56"/>
<instance part="NAP1" gate="+12" x="-63.5" y="-2.54"/>
<instance part="MOD5" gate="G$1" x="-22.86" y="-71.12" rot="MR0"/>
<instance part="GND8" gate="ZEM" x="-7.62" y="-73.66"/>
<instance part="NAP8" gate="+5" x="-10.16" y="-55.88"/>
<instance part="K8" gate="K" x="101.6" y="10.16" smashed="yes" rot="MR180">
<attribute name="NAME" x="107.3335" y="10.161" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="K15" gate="K" x="101.6" y="17.78" smashed="yes" rot="MR180">
<attribute name="NAME" x="107.3335" y="17.781" size="1.778" layer="95" rot="MR180"/>
</instance>
<instance part="K16" gate="K" x="10.16" y="40.64" smashed="yes" rot="MR0">
<attribute name="NAME" x="12.0465" y="43.179" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="12.0465" y="33.661" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="NAP5" gate="+5" x="-55.88" y="2.54"/>
<instance part="NAP7" gate="+12" x="88.9" y="5.08" smashed="yes">
<attribute name="VALUE" x="88.265" y="6.6675" size="1.778" layer="96"/>
</instance>
<instance part="GND2" gate="ZEM" x="93.98" y="-5.08"/>
<instance part="+3V1" gate="G$1" x="78.74" y="-12.7" smashed="yes">
<attribute name="VALUE" x="81.28" y="-12.7" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="K1" gate="K" x="101.6" y="-35.56" smashed="yes">
<attribute name="NAME" x="99.7135" y="-33.021" size="1.778" layer="95"/>
</instance>
</instances>
<busses>
<bus name="B$1">
<segment>
<wire x1="50.8" y1="2.54" x2="50.8" y2="-43.18" width="0.762" layer="92"/>
<wire x1="50.8" y1="-43.18" x2="10.16" y2="-43.18" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="SCK" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PI_1/D13/SPI2SCK/LED1"/>
<label x="43.18" y="2.54" size="1.778" layer="95"/>
<wire x1="33.02" y1="2.54" x2="50.8" y2="2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB_14/D12/SPI2MISO/PWM12"/>
<label x="43.18" y="0" size="1.778" layer="95"/>
<wire x1="33.02" y1="0" x2="50.8" y2="0" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB_15/D11/SPI2MOSI/PWM12"/>
<label x="43.18" y="-2.54" size="1.778" layer="95"/>
<wire x1="33.02" y1="-2.54" x2="50.8" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="RST" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PI_2/D8/SPI2MISO/PWM8"/>
<label x="43.18" y="-10.16" size="1.778" layer="95"/>
<wire x1="33.02" y1="-10.16" x2="50.8" y2="-10.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="STCK" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA_15/D9/PWM2"/>
<label x="43.18" y="-7.62" size="1.778" layer="95"/>
<wire x1="33.02" y1="-7.62" x2="50.8" y2="-7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CS1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA_8/D10/PWM1"/>
<label x="43.18" y="-5.08" size="1.778" layer="95"/>
<wire x1="33.02" y1="-5.08" x2="50.8" y2="-5.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CS2" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PI_3/D7/SPI2MOSI"/>
<label x="43.18" y="-15.24" size="1.778" layer="95"/>
<wire x1="33.02" y1="-15.24" x2="50.8" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CS3" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PH_6/D6/SPI5SCK/PWM12"/>
<label x="43.18" y="-17.78" size="1.778" layer="95"/>
<wire x1="33.02" y1="-17.78" x2="50.8" y2="-17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BUSY" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PG_7/D4"/>
<label x="43.18" y="-22.86" size="1.778" layer="95"/>
<wire x1="33.02" y1="-22.86" x2="50.8" y2="-22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FLAG" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PG_6/D2"/>
<label x="43.18" y="-27.94" size="1.778" layer="95"/>
<wire x1="33.02" y1="-27.94" x2="50.8" y2="-27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PA0" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PA_0/A0/PWM2/SERIAL4_TX"/>
<label x="-60.96" y="-20.32" size="1.778" layer="95" rot="MR0" xref="yes"/>
<wire x1="-60.96" y1="-20.32" x2="-50.8" y2="-20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="K12" gate="K" pin="K1"/>
<wire x1="96.52" y1="-15.24" x2="88.9" y2="-15.24" width="0.1524" layer="91"/>
<label x="88.9" y="-15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="GND@1"/>
<wire x1="-58.42" y1="-10.16" x2="-50.8" y2="-10.16" width="0.1524" layer="91"/>
<pinref part="GND4" gate="ZEM" pin="GND"/>
<pinref part="IC1" gate="G$1" pin="GND@2"/>
<wire x1="-50.8" y1="-10.16" x2="-50.8" y2="-12.7" width="0.1524" layer="91"/>
<junction x="-50.8" y="-10.16"/>
</segment>
<segment>
<pinref part="MOD5" gate="G$1" pin="GND"/>
<wire x1="-12.7" y1="-68.58" x2="-7.62" y2="-68.58" width="0.1524" layer="91"/>
<pinref part="GND8" gate="ZEM" pin="GND"/>
<wire x1="-7.62" y1="-71.12" x2="-7.62" y2="-68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND2" gate="ZEM" pin="GND"/>
<pinref part="K10" gate="K" pin="K2"/>
<wire x1="93.98" y1="-2.54" x2="96.52" y2="-2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND1" gate="ZEM" pin="GND"/>
<wire x1="17.78" y1="38.1" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
<pinref part="K4" gate="K" pin="K2"/>
<pinref part="K6" gate="K" pin="K2"/>
<wire x1="-10.16" y1="38.1" x2="-2.54" y2="38.1" width="0.1524" layer="91"/>
<junction x="-2.54" y="38.1"/>
<pinref part="K2" gate="K" pin="K2"/>
<wire x1="-2.54" y1="38.1" x2="5.08" y2="38.1" width="0.1524" layer="91"/>
<junction x="5.08" y="38.1"/>
<pinref part="K5" gate="K" pin="K2"/>
<wire x1="-10.16" y1="38.1" x2="-17.78" y2="38.1" width="0.1524" layer="91"/>
<junction x="-10.16" y="38.1"/>
<wire x1="-17.78" y1="38.1" x2="-20.32" y2="38.1" width="0.1524" layer="91"/>
<junction x="-17.78" y="38.1"/>
<pinref part="K16" gate="K" pin="K2"/>
<wire x1="5.08" y1="38.1" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
<junction x="15.24" y="38.1"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="GND@3"/>
<wire x1="33.02" y1="5.08" x2="43.18" y2="5.08" width="0.1524" layer="91"/>
<wire x1="43.18" y1="5.08" x2="43.18" y2="10.16" width="0.1524" layer="91"/>
<pinref part="K8" gate="K" pin="K1"/>
<wire x1="43.18" y1="10.16" x2="96.52" y2="10.16" width="0.1524" layer="91"/>
<label x="91.44" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<pinref part="K2" gate="K" pin="K1"/>
<pinref part="NAP3" gate="+12" pin="+12V"/>
<wire x1="17.78" y1="40.64" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<junction x="17.78" y="40.64"/>
<pinref part="K16" gate="K" pin="K1"/>
<pinref part="K5" gate="K" pin="K1"/>
<pinref part="K4" gate="K" pin="K1"/>
<wire x1="-17.78" y1="40.64" x2="-10.16" y2="40.64" width="0.1524" layer="91"/>
<pinref part="K6" gate="K" pin="K1"/>
<wire x1="-10.16" y1="40.64" x2="-2.54" y2="40.64" width="0.1524" layer="91"/>
<junction x="-10.16" y="40.64"/>
<wire x1="-2.54" y1="40.64" x2="5.08" y2="40.64" width="0.1524" layer="91"/>
<junction x="-2.54" y="40.64"/>
<junction x="5.08" y="40.64"/>
<wire x1="5.08" y1="40.64" x2="15.24" y2="40.64" width="0.1524" layer="91"/>
<junction x="15.24" y="40.64"/>
</segment>
<segment>
<pinref part="NAP1" gate="+12" pin="+12V"/>
<wire x1="-63.5" y1="-7.62" x2="-63.5" y2="-15.24" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VIN"/>
<wire x1="-63.5" y1="-15.24" x2="-50.8" y2="-15.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="K10" gate="K" pin="K1"/>
<wire x1="96.52" y1="0" x2="88.9" y2="0" width="0.1524" layer="91"/>
<pinref part="NAP7" gate="+12" pin="+12V"/>
</segment>
</net>
<net name="PI0" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PI_0/D5/SPI2NSS"/>
<wire x1="33.02" y1="-20.32" x2="35.56" y2="-20.32" width="0.1524" layer="91"/>
<label x="35.56" y="-20.32" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="K1" gate="K" pin="K2"/>
<wire x1="96.52" y1="-38.1" x2="81.28" y2="-38.1" width="0.1524" layer="91"/>
<label x="81.28" y="-38.1" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PF9" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PF_9/A2/PWM14/SPI5MOSI"/>
<wire x1="-60.96" y1="-25.4" x2="-50.8" y2="-25.4" width="0.1524" layer="91"/>
<label x="-60.96" y="-25.4" size="1.778" layer="95" rot="MR0" xref="yes"/>
</segment>
<segment>
<pinref part="K14" gate="K" pin="K2"/>
<wire x1="96.52" y1="-27.94" x2="81.28" y2="-27.94" width="0.1524" layer="91"/>
<label x="81.28" y="-27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="MOD5" gate="G$1" pin="5V"/>
<wire x1="-12.7" y1="-71.12" x2="-10.16" y2="-71.12" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="-71.12" x2="-10.16" y2="-60.96" width="0.1524" layer="91"/>
<pinref part="NAP8" gate="+5" pin="+5V"/>
</segment>
<segment>
<pinref part="IC1" gate="G$1" pin="+5V"/>
<pinref part="NAP5" gate="+5" pin="+5V"/>
<wire x1="-50.8" y1="-7.62" x2="-55.88" y2="-7.62" width="0.1524" layer="91"/>
<wire x1="-55.88" y1="-7.62" x2="-55.88" y2="-2.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PF_7/A4/PWM_11/SPI5SCK/SERIAL7TX"/>
<wire x1="-60.96" y1="-30.48" x2="-50.8" y2="-30.48" width="0.1524" layer="91"/>
<label x="-60.96" y="-30.48" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-12.7" y1="-63.5" x2="-7.62" y2="-63.5" width="0.1524" layer="91"/>
<pinref part="MOD5" gate="G$1" pin="RX"/>
<label x="-7.62" y="-63.5" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PF_6/A5/PWM10/SPI5NSS/SERIAL7RX"/>
<wire x1="-53.34" y1="-33.02" x2="-50.8" y2="-33.02" width="0.1524" layer="91"/>
<label x="-53.34" y="-33.02" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="-12.7" y1="-66.04" x2="-2.54" y2="-66.04" width="0.1524" layer="91"/>
<pinref part="MOD5" gate="G$1" pin="TX"/>
<label x="-2.54" y="-66.04" size="1.778" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PC_7/D0/SERIAL6RX/PWM8"/>
<wire x1="33.02" y1="-33.02" x2="50.8" y2="-33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PF10" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PF_10/A1"/>
<label x="-53.34" y="-22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-50.8" y1="-22.86" x2="-53.34" y2="-22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="K13" gate="K" pin="K1"/>
<wire x1="96.52" y1="-45.72" x2="91.44" y2="-45.72" width="0.1524" layer="91"/>
<label x="91.44" y="-45.72" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PF8" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PF_8/A3/PWM13/SPI5MISO"/>
<label x="-53.34" y="-27.94" size="1.778" layer="95" rot="MR0" xref="yes"/>
<wire x1="-50.8" y1="-27.94" x2="-53.34" y2="-27.94" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="K14" gate="K" pin="K1"/>
<wire x1="96.52" y1="-25.4" x2="91.44" y2="-25.4" width="0.1524" layer="91"/>
<label x="91.44" y="-25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PB4" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB_4/D3/SPI2NSS/PWM3"/>
<wire x1="33.02" y1="-25.4" x2="35.56" y2="-25.4" width="0.1524" layer="91"/>
<label x="35.56" y="-25.4" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="K1" gate="K" pin="K1"/>
<wire x1="96.52" y1="-35.56" x2="91.44" y2="-35.56" width="0.1524" layer="91"/>
<label x="91.44" y="-35.56" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PC6" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PC_6/D1/SERIAL6TX/PWM8"/>
<wire x1="33.02" y1="-30.48" x2="35.56" y2="-30.48" width="0.1524" layer="91"/>
<label x="35.56" y="-30.48" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<pinref part="K13" gate="K" pin="K2"/>
<wire x1="96.52" y1="-48.26" x2="83.82" y2="-48.26" width="0.1524" layer="91"/>
<label x="83.82" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="SCL" class="0">
<segment>
<pinref part="K15" gate="K" pin="K2"/>
<pinref part="IC1" gate="G$1" pin="PB_8/D15/I2C1SCL/PWM4"/>
<wire x1="96.52" y1="20.32" x2="33.02" y2="20.32" width="0.1524" layer="91"/>
<wire x1="33.02" y1="20.32" x2="33.02" y2="12.7" width="0.1524" layer="91"/>
<label x="91.44" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="SDA" class="0">
<segment>
<pinref part="IC1" gate="G$1" pin="PB_9/D14/I2C1SDA/PWM4/SPI2NSS"/>
<wire x1="33.02" y1="10.16" x2="35.56" y2="10.16" width="0.1524" layer="91"/>
<wire x1="35.56" y1="10.16" x2="35.56" y2="17.78" width="0.1524" layer="91"/>
<pinref part="K15" gate="K" pin="K1"/>
<wire x1="35.56" y1="17.78" x2="96.52" y2="17.78" width="0.1524" layer="91"/>
<label x="91.44" y="17.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="K8" gate="K" pin="K2"/>
<wire x1="96.52" y1="12.7" x2="40.64" y2="12.7" width="0.1524" layer="91"/>
<wire x1="40.64" y1="12.7" x2="40.64" y2="7.62" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="AVDD"/>
<wire x1="40.64" y1="7.62" x2="33.02" y2="7.62" width="0.1524" layer="91"/>
<label x="91.44" y="12.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="K12" gate="K" pin="K2"/>
<pinref part="+3V1" gate="G$1" pin="+3V3"/>
<wire x1="96.52" y1="-17.78" x2="78.74" y2="-17.78" width="0.1524" layer="91"/>
<wire x1="78.74" y1="-17.78" x2="78.74" y2="-15.24" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
