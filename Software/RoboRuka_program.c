//ROBORUKA NA DISCO-746NG
#include "mbed.h"
#include "PowerSTEP01.h"
#include "math.h"
#include "TS_DISCO_F746NG.h"
#include "LCD_DISCO_F746NG.h"

#define M_PI 3.14159265358979323846
#define LCD_COLOR_BLUE          ((uint32_t)0xFF0000FF)
#define LCD_COLOR_GREEN         ((uint32_t)0xFF00FF00)
#define LCD_COLOR_RED           ((uint32_t)0xFFFF0000)
#define LCD_COLOR_CYAN          ((uint32_t)0xFF00FFFF)
#define LCD_COLOR_MAGENTA       ((uint32_t)0xFFFF00FF)
#define LCD_COLOR_YELLOW        ((uint32_t)0xFFFFFF00)
#define LCD_COLOR_LIGHTBLUE     ((uint32_t)0xFF8080FF)
#define LCD_COLOR_LIGHTGREEN    ((uint32_t)0xFF80FF80)
#define LCD_COLOR_LIGHTRED      ((uint32_t)0xFFFF8080)
#define LCD_COLOR_LIGHTCYAN     ((uint32_t)0xFF80FFFF)
#define LCD_COLOR_LIGHTMAGENTA  ((uint32_t)0xFFFF80FF)
#define LCD_COLOR_LIGHTYELLOW   ((uint32_t)0xFFFFFF80)
#define LCD_COLOR_DARKBLUE      ((uint32_t)0xFF000080)
#define LCD_COLOR_DARKGREEN     ((uint32_t)0xFF008000)
#define LCD_COLOR_DARKRED       ((uint32_t)0xFF800000)
#define LCD_COLOR_DARKCYAN      ((uint32_t)0xFF008080)
#define LCD_COLOR_DARKMAGENTA   ((uint32_t)0xFF800080)
#define LCD_COLOR_DARKYELLOW    ((uint32_t)0xFF808000)
#define LCD_COLOR_WHITE         ((uint32_t)0xFFFFFFFF)
#define LCD_COLOR_LIGHTGRAY     ((uint32_t)0xFFD3D3D3)
#define LCD_COLOR_GRAY          ((uint32_t)0xFF808080)
#define LCD_COLOR_DARKGRAY      ((uint32_t)0xFF404040)
#define LCD_COLOR_BLACK         ((uint32_t)0xFF000000)
#define LCD_COLOR_BROWN         ((uint32_t)0xFFA52A2A)
#define LCD_COLOR_ORANGE        ((uint32_t)0xFFFFA500)
#define LCD_COLOR_TRANSPARENT   ((uint32_t)0xFF000000)
#define MANUAL x>10  && x<94  && y>38 && y<123
#define HOMING x>104 && x<188 && y>38 && y<123
#define UCENI  x>198 && x<282 && y>38  && y<123
#define REMOTE_CONTROL  x>292 && x<376 && y>38  && y<123
#define PROGRAM1        x>386 && x<470 && y>38  && y<123
#define PROGRAM2        x>10  && x<94  && y>153 && y<238
#define PROGRAM3        x>104 && x<188 && y>153 && y<238
#define PROGRAM4        x>198 && x<282 && y>153 && y<238
#define PROGRAM5        x>292 && x<376 && y>153 && y<238
#define PROGRAM6        x>386 && x<470 && y>153 && y<238
#define M_ROTACE_DOLEVA     x>10  && x<94  && y>113 && y<198
#define M_ROTACE_DOPRAVA    x>10  && x<94  && y>18  && y<103
#define M_ZVEDANI_NAHORU    x>104 && x<188 && y>18  && y<103
#define M_ZVEDANI_DOLU      x>104 && x<188 && y>113 && y<198
#define M_RAMENO_NAHORU     x>198 && x<282 && y>18  && y<103
#define M_RAMENO_DOLU       x>198 && x<282 && y>113 && y<198
#define M_KLESTE_OTEVRIT    x>292 && x<376 && y>18  && y<103
#define M_KLESTE_ZAVRIT     x>292 && x<376 && y>113 && y<198
#define M_RYCHLOST_PLUS     x>386 && x<470 && y>18  && y<103
#define M_RYCHLOST_MINUS    x>386 && x<470 && y>113 && y<198
#define M_POS1              x>10  && x<94  && y>198 && y<272
#define M_POS2              x>104 && x<188 && y>198 && y<272
#define M_POS3              x>198 && x<282 && y>198 && y<272
#define M_POS4              x>292 && x<376 && y>198 && y<272             
#define M_HOME              x>386 && x<470 && y>198 && y<272
#define SAVE                x>10  && x<376 && y>225 && y<265
#define EXIT                x>386 && x<470 && y>225 && y<265            
DigitalIn    TLA(PI_11);

DigitalIn    FLAG (D2);
DigitalIn    BUSY (D4);
DigitalIn    DRDY (D6);
DigitalOut   RST  (D8); 
DigitalOut   STCK (D9); 
DigitalOut   CS1  (D10);
DigitalOut   CS2  (D7);
DigitalOut   CS3  (D6);
DigitalOut   CS4  (D0);

SPI          spi(D11, D12, D13); // mosi, miso, sclk
Serial       pc(USBTX, USBRX, 115200);

Serial BT (PF_7, PF_6, 115200);
I2C i2c(PB_9, PB_8);  // SDA, SCL

LCD_DISCO_F746NG lcd;
TS_DISCO_F746NG ts;
TS_StateTypeDef TS_State;

Ticker       Sekunda;
Timer        Stopky;

void Nastav_Gyro();
void TiskPoSekunde();
void Homing();
void Manual();
void Motor_1_uceni();
void Motor_2_uceni();
void Motor_3_uceni();
void Motor_4_uceni();
void UvodniObrazovka();
void ts_check_fun();
int  NactiDotek();
int obrazovka=1;
void Button(int pozice_x,int pozice_y, int sirka, int vyska,unsigned long barva, unsigned long ramecek);
void Text(int poz_x, int poz_y,int font,unsigned long barva_textu,unsigned long barva_pozadi,char *text);
void Cislo(int poz_x,int poz_y, int font,unsigned long barva_textu,unsigned long barva_pozadi,long cislo);
void VytiskniGyro(int poz_x, int poz_y,int font,unsigned long barva_textu,unsigned long barva_pozadi);
void VytiskniPozice(int poz_x, int poz_y,int font,unsigned long barva_textu,unsigned long barva_pozadi);
void Precti_Gyro();
void RemoteControl();
void Uceni();
void Ukladani();
void Program1();
void Program2();
void Program3();
void Program4();
void Program5();
void Program6();
int pocet_doteku=0, predchozi_pocet_doteku=0, prvni_dotek=0, pastX, pastY, x, y,
krok=0,step1=0,step2=0,step3=0,step4=0,program1_step, program2_step, 
program3_step, program4_step, program5_step, program6_step;
long Gyro_X, Gyro_Y, Gyro_Z, POS1, POS2, POS3, POS4,
pozice1[100],pozice2[100], pozice3[100], pozice4[100], program1[5][100],
program2[5][100],
program3[5][100],program4[5][100],program5[5][100],program6[5][100] ;
double Naklon, Rotace;
long ACC_X, ACC_Y, ACC_Z, TEMP;
unsigned long barva_tlacitka = LCD_COLOR_YELLOW, barva_textu_pozadi = LCD_COLOR_WHITE, 
barva_textu_tlacitko = LCD_COLOR_BLACK, barva_pozadi = LCD_COLOR_BLUE; 
double Ax, Ay, Az, Ax2, Ay2, Az2;
char MPU6050[20], I2C_cmd[10];
    
long spd=20000;
bool sekunda_flag=false, konec_flag=false;
int32_t celkova_pozice=0, pozice=0, minula_pozice=0, preteceni_pozice=0, enkoder=0;
int32_t vysledek2, CisloMereni=0;
double rychlost, interval;
double diag, cas;
int i;
uint8_t text[50];

unsigned char BTread[10];


void ts_check_fun()
{
    ts.GetState(&TS_State);
    pocet_doteku = TS_State.touchDetected;
}

  
/*****************************************************************************/
/*********************** OBSLUHA DRIVERU POWERSTEP01 *************************/
/*****************************************************************************/

 void driver_spi(uint8_t* ReadBuffer, uint8_t* WriteBuffer, DigitalOut CS)
    {
        CS = 0;
        wait_us(1);
        ReadBuffer[0] = spi.write(WriteBuffer[0]);
        CS = 1;
        wait_us(1);
    }   

/**************************************************************************************************************************************************/

uint32_t ReadDriverRegister(uint8_t RegisterCode, DigitalOut DriverCSpin)
{
    uint32_t spiRxData;
    uint8_t Readspi[10], Writespi[10];
  
    for(int i=0; i<10; i++) {Readspi[i] = 0; Writespi[i] = 0;} 
    
    Writespi[0]=RegisterCode+32;
  
  switch (RegisterCode)
    {
      case POWERSTEP01_ABS_POS: 
      case POWERSTEP01_MARK:
      case POWERSTEP01_SPEED:
            driver_spi(&Readspi[0], &Writespi[0], DriverCSpin); 
            driver_spi(&Readspi[1], &Writespi[1], DriverCSpin);
            driver_spi(&Readspi[2], &Writespi[2], DriverCSpin);
            driver_spi(&Readspi[3], &Writespi[3], DriverCSpin);
            spiRxData = ((uint32_t)Readspi[1] << 16)| (Readspi[2] << 8) | (Readspi[3]);
            break;
      case POWERSTEP01_EL_POS:
      case POWERSTEP01_ACC:
      case POWERSTEP01_DEC:
      case POWERSTEP01_MAX_SPEED:
      case POWERSTEP01_MIN_SPEED:
      case POWERSTEP01_FS_SPD:
      case POWERSTEP01_INT_SPEED:
      case POWERSTEP01_CONFIG:
      case POWERSTEP01_GATECFG1:
      case POWERSTEP01_STATUS:
            driver_spi(&Readspi[0], &Writespi[0], DriverCSpin); 
            driver_spi(&Readspi[1], &Writespi[1], DriverCSpin);
            driver_spi(&Readspi[2], &Writespi[2], DriverCSpin);
            spiRxData = ((uint32_t)Readspi[1] << 8)| (Readspi[2]); 
            break;
    default:
            driver_spi(&Readspi[0], &Writespi[0], DriverCSpin); 
            driver_spi(&Readspi[1], &Writespi[1], DriverCSpin);
            spiRxData = (uint32_t)Readspi[1]; 
  }
return spiRxData;          
}

/**************************************************************************************************************************************************/

void WriteDriverCommand(uint8_t CommandCode, uint32_t CommandValue, DigitalOut DriverCSpin)
{
    uint8_t Readspi[10], Writespi[10];
    for(int i=0; i<10; i++) {Readspi[i] = 0; Writespi[i] = 0;} 
    
    Writespi[0]=CommandCode;
  
  switch (CommandCode)
    {
      case POWERSTEP01_ABS_POS: 
      case POWERSTEP01_MARK:
      case POWERSTEP01_RUN_FWD:
      case POWERSTEP01_RUN_REV:
      case POWERSTEP01_MOVE_FWD: 
      case POWERSTEP01_MOVE_REV: 
      case POWERSTEP01_GO_TO:
      case POWERSTEP01_GO_TO_REV:          
      case POWERSTEP01_GO_TO_FWD:          
      case POWERSTEP01_GO_UNTIL_FWD_RST:           
      case POWERSTEP01_GO_UNTIL_REV_RST:           
      case POWERSTEP01_GO_UNTIL_FWD_MARK:           
      case POWERSTEP01_GO_UNTIL_REV_MARK:           
            Writespi[1] = (uint8_t)(CommandValue >> 16);
            Writespi[2] = (uint8_t)(CommandValue >> 8);
            Writespi[3] = (uint8_t) CommandValue;
            driver_spi(&Readspi[0], &Writespi[0], DriverCSpin); 
            driver_spi(&Readspi[1], &Writespi[1], DriverCSpin); 
            driver_spi(&Readspi[2], &Writespi[2], DriverCSpin); 
            driver_spi(&Readspi[3], &Writespi[3], DriverCSpin); 
            driver_spi(&Readspi[4], &Writespi[4], DriverCSpin); 
            break;
      case POWERSTEP01_EL_POS:
      case POWERSTEP01_ACC:
      case POWERSTEP01_DEC:
      case POWERSTEP01_MAX_SPEED:
      case POWERSTEP01_MIN_SPEED:
      case POWERSTEP01_FS_SPD:
      case POWERSTEP01_INT_SPEED:
      case POWERSTEP01_CONFIG:
      case POWERSTEP01_GATECFG1:
            Writespi[1] = (uint8_t)(CommandValue >> 8);
            Writespi[2] = (uint8_t) CommandValue;
            driver_spi(&Readspi[0], &Writespi[0], DriverCSpin); 
            driver_spi(&Readspi[1], &Writespi[1], DriverCSpin); 
            driver_spi(&Readspi[2], &Writespi[2], DriverCSpin);
            driver_spi(&Readspi[3], &Writespi[3], DriverCSpin); 
            break;
    case POWERSTEP01_ADC_OUT:
    case POWERSTEP01_OCD_TH:
    case POWERSTEP01_STEP_MODE:
    case POWERSTEP01_ALARM_EN:
    case POWERSTEP01_GATECFG2:
    case POWERSTEP01_KVAL_HOLD:
    case POWERSTEP01_KVAL_RUN:
    case POWERSTEP01_KVAL_ACC:
    case POWERSTEP01_KVAL_DEC:
    case POWERSTEP01_ST_SLP:
    case POWERSTEP01_FN_SLP_ACC:
    case POWERSTEP01_FN_SLP_DEC:
    case POWERSTEP01_K_THERM:
    case POWERSTEP01_STALL_TH:
            Writespi[1] = (uint8_t) CommandValue;
            driver_spi(&Readspi[0], &Writespi[0], DriverCSpin); 
            driver_spi(&Readspi[1], &Writespi[1], DriverCSpin); 
            driver_spi(&Readspi[2], &Writespi[2], DriverCSpin);
            break;
    default:
            driver_spi(&Readspi[0], &Writespi[0], DriverCSpin); 
            driver_spi(&Readspi[1], &Writespi[1], DriverCSpin);
  } 
}

/**************************************************************************************************************************************************/

uint32_t GetDriverStatus(DigitalOut DriverCSpin)
{
    uint32_t spiRxData;
    uint8_t Readspi[10], Writespi[10];
  
    for(int i=0; i<10; i++) {Readspi[i] = 0; Writespi[i] = 0;} 
    Writespi[0]=0xD0;
    
    driver_spi(&Readspi[0], &Writespi[0], DriverCSpin); 
    driver_spi(&Readspi[1], &Writespi[1], DriverCSpin);
    driver_spi(&Readspi[2], &Writespi[2], DriverCSpin);
    spiRxData = ((uint32_t)Readspi[1] << 8)| (Readspi[2]); 
    
    return spiRxData;
}

/**************************************************************************************************************************************************/

void SettingDrivers()
{
    WriteDriverCommand(POWERSTEP01_ALARM_EN, 0b10000110, DRIVER1);    // Alarmy: thermal, command error
    WriteDriverCommand(POWERSTEP01_STEP_MODE, 0b00000111, DRIVER1);    // BUSY output, voltage mode, 128 microsteps
    WriteDriverCommand(POWERSTEP01_CONFIG, 0x0000, DRIVER1);    // 19.5kHz PWM, 7.5V VCC/UVLO, nestop pri nadproudu, bez kompenzace BEMF, SW vyvola HardStop, interni oscilator 16 MHz
    WriteDriverCommand(POWERSTEP01_MIN_SPEED, 7032, DRIVER1); // Nizkorychlostni optimalizace pod 700 kroku/s
    WriteDriverCommand(POWERSTEP01_MAX_SPEED, 20, DRIVER1); // Nizkorychlostni optimalizace pod 700 kroku/s
    WriteDriverCommand(POWERSTEP01_FS_SPD, 1023, DRIVER1); // Neprepinat z mikrostep na fullstep pri prekroceni limitni rychlosti
    WriteDriverCommand(POWERSTEP01_KVAL_HOLD, 255, DRIVER1); // Amplituda napeti v zabrzdenem stavu (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_RUN, 255, DRIVER1); // Amplituda napeti v chodu (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_ACC, 255, DRIVER1); // Amplituda napeti pri zrychleni (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_DEC, 255, DRIVER1); // Amplituda napeti pri zpomaleni (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_OCD_TH, 31, DRIVER1);    // Max. proud 1562 mA
    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1); 
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1); 
  
    
    WriteDriverCommand(POWERSTEP01_ALARM_EN, 0b10000110, DRIVER2);    // Alarmy: thermal, command error
    WriteDriverCommand(POWERSTEP01_STEP_MODE, 0b00000111, DRIVER2);    // BUSY output, voltage mode, 128 microsteps
    WriteDriverCommand(POWERSTEP01_CONFIG, 0x0000, DRIVER2);    // 19.5kHz PWM, 7.5V VCC/UVLO, nestop pri nadproudu, bez kompenzace BEMF, SW vyvola HardStop, interni oscilator 16 MHz
    WriteDriverCommand(POWERSTEP01_MIN_SPEED, 7032, DRIVER2); // Nizkorychlostni optimalizace pod 700 kroku/s
    WriteDriverCommand(POWERSTEP01_FS_SPD, 1023, DRIVER2); // Neprepinat z mikrostep na fullstep pri prekroceni limitni rychlosti
    WriteDriverCommand(POWERSTEP01_KVAL_HOLD, 255, DRIVER2); // Amplituda napeti v zabrzdenem stavu (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_RUN, 255, DRIVER2); // Amplituda napeti v chodu (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_ACC, 255, DRIVER2); // Amplituda napeti pri zrychleni (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_DEC, 255, DRIVER2); // Amplituda napeti pri zpomaleni (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_OCD_TH, 31, DRIVER2);    // Max. proud 1562 mA
    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER2);  
  
    
    WriteDriverCommand(POWERSTEP01_ALARM_EN, 0b10000110, DRIVER3);    // Alarmy: thermal, command error
    WriteDriverCommand(POWERSTEP01_STEP_MODE, 0b00000111, DRIVER3);    // BUSY output, voltage mode, 128 microsteps
    WriteDriverCommand(POWERSTEP01_CONFIG, 0x0000, DRIVER3);    // 19.5kHz PWM, 7.5V VCC/UVLO, nestop pri nadproudu, bez kompenzace BEMF, SW vyvola HardStop, interni oscilator 16 MHz
    WriteDriverCommand(POWERSTEP01_MIN_SPEED, 7032, DRIVER3); // Nizkorychlostni optimalizace pod 700 kroku/s
    WriteDriverCommand(POWERSTEP01_FS_SPD, 1023, DRIVER3); // Neprepinat z mikrostep na fullstep pri prekroceni limitni rychlosti
    WriteDriverCommand(POWERSTEP01_KVAL_HOLD, 255, DRIVER3); // Amplituda napeti v zabrzdenem stavu (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_RUN, 255, DRIVER3); // Amplituda napeti v chodu (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_ACC, 255, DRIVER3); // Amplituda napeti pri zrychleni (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_DEC, 255, DRIVER3); // Amplituda napeti pri zpomaleni (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_OCD_TH, 31, DRIVER3);    // Max. proud 1562 mA
    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3); 
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3); 
  
    WriteDriverCommand(POWERSTEP01_ALARM_EN, 0b10000110, DRIVER4);    // Alarmy: thermal, command error
    WriteDriverCommand(POWERSTEP01_STEP_MODE, 0b00000111, DRIVER4);    // BUSY output, voltage mode, 128 microsteps
    WriteDriverCommand(POWERSTEP01_CONFIG, 0x0000, DRIVER4);    // 19.5kHz PWM, 7.5V VCC/UVLO, nestop pri nadproudu, bez kompenzace BEMF, SW vyvola HardStop, interni oscilator 16 MHz
    WriteDriverCommand(POWERSTEP01_MIN_SPEED, 7032, DRIVER4); // Nizkorychlostni optimalizace pod 700 kroku/s
    WriteDriverCommand(POWERSTEP01_FS_SPD, 1023, DRIVER4); // Neprepinat z mikrostep na fullstep pri prekroceni limitni rychlosti
    WriteDriverCommand(POWERSTEP01_KVAL_HOLD, 255, DRIVER4); // Amplituda napeti v zabrzdenem stavu (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_RUN, 255, DRIVER4); // Amplituda napeti v chodu (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_ACC, 255, DRIVER4); // Amplituda napeti pri zrychleni (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_KVAL_DEC, 255, DRIVER4); // Amplituda napeti pri zpomaleni (cca 1.5A/12V napajeni)
    WriteDriverCommand(POWERSTEP01_OCD_TH, 31, DRIVER4);    // Max. proud 1562 mA
    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4); 
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4); 

}


/**************************************************************************************************************************************************/
/*********************************** OBSLUHA PRERUSENI *********************************************** OBSLUHA PRERUSENI **************************/
/**************************************************************************************************************************************************/

void TiskPoSekunde()
{
  sekunda_flag = true;
}

void KonecProgramu()
{
  WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1); 
  WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1); 
  WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1); 
  konec_flag = true;
}

/**************************************************************************************************************************************************/
/*********************************** HLAVNI PROGRAM  *********************************************** HLAVNI PROGRAM  ******************************/
/**************************************************************************************************************************************************/

int main()
{
  CS1=1; CS2=1; CS3=1; CS4=1; RST=1; STCK=0;
  
   pc.printf("\r\nSTART PROGRAMU\r\n");
   
   spi.format(8,3);  // spi PowerStep01
   spi.frequency(1000000);
   
   i2c.frequency(100000);
  
//  KS_STOP.fall(&KonecProgramu);
  
   pc.printf("\r\nSTART PROGRAMU\r\n");
   wait_ms(10);
   SettingDrivers();
    Nastav_Gyro();
 
    ts.Init(lcd.GetXSize(), lcd.GetYSize());
 //   ts_check.attach(&ts_check_fun, 0.1);
   
   while(1)
   {
     switch(obrazovka)
     {
        case 1: UvodniObrazovka();
            break;
        case 2: Manual();
            break;
        case 3: Homing();
            break;
        case 4: Uceni();
            break;
        case 5: RemoteControl();
            break;
        case 6: Program1();
            break;
        case 7: Program2();
            break;
        case 8: Program3();
            break;
        case 9: Program4();
            break;
        case 10: Program5();
            break;
        case 11: Program6();
            break;
     }
   }
    

 
 
 
 
 /*
    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);  // vypne motor
    wait(0.5);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1); 
    WriteDriverCommand(POWERSTEP01_GO_TO_REV, 4000000, DRIVER1); 
    
    */
    
} 

void Button(int pozice_x, int pozice_y, int sirka, int vyska,unsigned long barva, unsigned long ramecek)
{
   lcd.SetTextColor(barva);   
   lcd.FillRect(pozice_x,pozice_y,sirka,vyska);
   lcd.SetTextColor(ramecek); lcd.DrawRect(pozice_x,pozice_y,sirka,vyska);  
}

void Text(int poz_x, int poz_y,int font,unsigned long barva_textu,unsigned long barva_pozadi,char *text)
{
  lcd.SetTextColor(barva_textu);
  lcd.SetBackColor(barva_pozadi);
      
  if(font==12) {lcd.SetFont(&Font12);}
  else if(font==16) {lcd.SetFont(&Font16);}
  else if(font==20) {lcd.SetFont(&Font20);}
  else if(font==24) {lcd.SetFont(&Font24);}
  else              {lcd.SetFont(&Font8);}
  lcd.DisplayStringAt(poz_x,poz_y, (uint8_t *)text, LEFT_MODE);
}

int NactiDotek()
{
   ts.GetState(&TS_State);
   if(TS_State.touchDetected > 0)
   { 
        x = TS_State.touchX[0];
        y = TS_State.touchY[0];
   }
   return TS_State.touchDetected;
}
void Cislo(int poz_x,int poz_y, int font,unsigned long barva_textu,unsigned long barva_pozadi,long cislo)
{    
    lcd.SetTextColor(barva_textu);
    lcd.SetBackColor(barva_pozadi);
    sprintf((char*)text, "%i", cislo);
    if(font==12) {lcd.SetFont(&Font12);}
    else if(font==16) {lcd.SetFont(&Font16);}
    else if(font==20) {lcd.SetFont(&Font20);}
    else if(font==24) {lcd.SetFont(&Font24);}
    else              {lcd.SetFont(&Font8);}
    lcd.DisplayStringAt(poz_x,poz_y,(uint8_t *)&text, LEFT_MODE);
    
}
void Nastav_Gyro()
{
 
    I2C_cmd[0] = 25; I2C_cmd[1] = 67; I2C_cmd[1] = 4; i2c.write(0xD0, I2C_cmd, 3);   // Registry 25 a 26: Rychlost 15 Hz, filtr bandwidth 21 Hz
    I2C_cmd[0] = 107; I2C_cmd[1] = 0; i2c.write(0xD0, I2C_cmd, 2);   // PowerManagemenent register - zapnuti mereni
    //default: rozsah gyro +/-250o/s, acc +/- 2g, no interrupts, no FIFO
    
    /*
    I2C_cmd[0] = 25; I2C_cmd[1] = 67; I2C_cmd[1] = 4; i2c.write(0xD1, I2C_cmd, 3);   // Registry 25 a 26: Rychlost 15 Hz, filtr bandwidth 21 Hz
    I2C_cmd[0] = 107; I2C_cmd[1] = 0; i2c.write(0xD1, I2C_cmd, 2);   // PowerManagemenent register - zapnuti mereni
    //default: rozsah gyro +/-250o/s, acc +/- 2g, no interrupts, no FIFO
    */
    
}


void Precti_Gyro()
{

    I2C_cmd[0] = 59; i2c.write(0xD0, I2C_cmd, 1);   // Nastaveni cteni na adresu 59
    i2c.read(0xD0, MPU6050, 14);
    
    
    ACC_X = (int)(MPU6050[0] * 256) + (int) MPU6050[1];
    ACC_Y = (int)(MPU6050[2] * 256) + (int) MPU6050[3];
    ACC_Z = (int)(MPU6050[4] * 256) + (int) MPU6050[5];
    TEMP  = (int)(MPU6050[6] * 256) + (int) MPU6050[7];
    Gyro_X= (int)(MPU6050[8] * 256) + (int) MPU6050[9];
    Gyro_Y= (int)(MPU6050[10] * 256) + (int) MPU6050[11];
    Gyro_Z= (int)(MPU6050[12] * 256) + (int) MPU6050[13];
    
    if(ACC_X > 32767) ACC_X = ACC_X - 65536;
    if(ACC_Y > 32767) ACC_Y = ACC_Y - 65536;
    if(ACC_Z > 32767) ACC_Z = ACC_Z - 65536;
    if(TEMP > 32767) TEMP = TEMP - 65536;
    if(Gyro_X > 32767) Gyro_X = Gyro_X - 65536;
    if(Gyro_Y > 32767) Gyro_Y = Gyro_Y - 65536;
    if(Gyro_Z > 32767) Gyro_Z = Gyro_Z - 65536;
    
    ACC_X = (ACC_X * 2000) / 32768;
    ACC_Y = (ACC_Y * 2000) / 32768;
    ACC_Z = (ACC_Z * 2000) / 32768;
    Gyro_X = (Gyro_X * 250) / 32768;
    Gyro_Y = (Gyro_Y * 250) / 32768;
    Gyro_Z = (Gyro_Z * 250) / 32768;
    TEMP = (TEMP * 1000) / 340 + 36530;
    
    Ax = (double)ACC_X / 1000.0; Ax2 = Ax * Ax;
    Ay = (double)ACC_Y / 1000.0; Ay2 = Ay * Ay;
    Az = (double)ACC_Z / 1000.0; Az2 = Az * Az;
       
    Rotace = 180.0 * atan(Ax/sqrt(Ay2 + Az2))/M_PI;   // podle D.Molloye
    Naklon = -180.0 * atan(Ay/sqrt(Ax2 + Az2))/M_PI;    // podle D.Molloye 
}


void VytiskniGyro(int poz_x, int poz_y,int font,unsigned long barva_textu,unsigned long barva_pozadi)
{
    
    lcd.ClearStringLine(14);
  
    lcd.SetTextColor(barva_textu);
    lcd.SetBackColor(barva_pozadi);
  //  sprintf((char*)text, "x%d y%d z%d n%d      x%d y%d z%d n%d", GYRO1_X,GYRO1_Y,GYRO1_Z,(long)Rotace1,Gyro_X,Gyro_Y,Gyro_Z,(long)Rotace);
    
    if(font==12) {lcd.SetFont(&Font12);}
    else if(font==16) {lcd.SetFont(&Font16);}
    else if(font==20) {lcd.SetFont(&Font20);}
    else if(font==24) {lcd.SetFont(&Font24);}
    else              {lcd.SetFont(&Font8);}

    
    sprintf((char*)text, "GYRO: x%d  ", Gyro_X);lcd.DisplayStringAt(10,LINE(14),(uint8_t *)&text, LEFT_MODE);
    sprintf((char*)text, "y%d  ", Gyro_Y);lcd.DisplayStringAt(135,LINE(14),(uint8_t *)&text, LEFT_MODE);
    sprintf((char*)text, "z%d  ", Gyro_Z);lcd.DisplayStringAt(185,LINE(14),(uint8_t *)&text, LEFT_MODE);
    sprintf((char*)text, "nkl:%d  ",(long)Naklon);lcd.DisplayStringAt(295,LINE(14),(uint8_t *)&text, LEFT_MODE);
    sprintf((char*)text, "rot:%d  ",(long)Rotace);lcd.DisplayStringAt(400,LINE(14),(uint8_t *)&text, LEFT_MODE);
    

        lcd.ClearStringLine(15);
    sprintf((char*)text, "x%d   y%d   z%d", ACC_X,ACC_Y,ACC_Z);
    lcd.DisplayStringAt(poz_x,LINE(15),(uint8_t *)&text, LEFT_MODE);

    
 //lcd.DisplayStringAt(poz_x,poz_y,(long)Naklon, LEFT_MODE);
}
void VytiskniPozice(int poz_x, int poz_y,int font,unsigned long barva_textu,unsigned long barva_pozadi)
{
    POS1 = ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
    POS2 = ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
    POS3 = ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
    POS4 = ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
    lcd.ClearStringLine(13);
    
    if(font==12) {lcd.SetFont(&Font12);}
    else if(font==16) {lcd.SetFont(&Font16);}
    else if(font==20) {lcd.SetFont(&Font20);}
    else if(font==24) {lcd.SetFont(&Font24);}
    else              {lcd.SetFont(&Font8);}
    Cislo(10,LINE(13),16,LCD_COLOR_WHITE,LCD_COLOR_BLUE, POS1);
    Cislo(104,LINE(13),16,LCD_COLOR_WHITE,LCD_COLOR_BLUE,POS2);
    Cislo(198,LINE(13),16,LCD_COLOR_WHITE,LCD_COLOR_BLUE,POS4);
    Cislo(292,LINE(13),16,LCD_COLOR_WHITE,LCD_COLOR_BLUE,POS3);
    Cislo(386,LINE(13),16,LCD_COLOR_WHITE,LCD_COLOR_BLUE,spd);
    
 //lcd.DisplayStringAt(poz_x,poz_y,(long)Naklon, LEFT_MODE);
}
void UvodniObrazovka()
{
    lcd.Clear(barva_pozadi);
    lcd.SetBackColor(barva_pozadi);
    ts.Init(lcd.GetXSize(), lcd.GetYSize()); 
    Text(60, LINE(0),24,barva_textu_pozadi,barva_pozadi,"ROBOTICKA RUKA FRENGP");
    
    Button(10,38,84,85, barva_tlacitka,barva_textu_tlacitko);      Text(20,70,16,barva_textu_tlacitko,barva_tlacitka,"MANUAL");
    Button(104,38,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(110,70,16,barva_textu_tlacitko,barva_tlacitka,"HOMING");
    Button(198,38,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(212,70,16,barva_textu_tlacitko,barva_tlacitka,"LEARN");
    Button(292,38,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(300,65,16,barva_textu_tlacitko,barva_tlacitka,"REMOTE");
                                                                   Text(295,81,16,barva_textu_tlacitko,barva_tlacitka,"CONTROL");
    Button(386,38,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(420,70,16,barva_textu_tlacitko,barva_tlacitka,"1");
         
    Button(10,153,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(45,190,16,barva_textu_tlacitko, barva_tlacitka,"2");
    Button(104,153,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(134,190,16,barva_textu_tlacitko,barva_tlacitka,"3"); 
    Button(198,153,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(227,190,16,barva_textu_tlacitko,barva_tlacitka,"4");
    Button(292,153,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(330,190,16,barva_textu_tlacitko,barva_tlacitka,"5");
    Button(386,153,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(420,190,16,barva_textu_tlacitko,barva_tlacitka,"6");
    
    while(1)
    {
     if(NactiDotek()>0)
     {
        if(MANUAL)             {obrazovka=2;return;} 
        else if(HOMING)        {obrazovka=3;return;}
        else if(UCENI)         {obrazovka=4;return;}
        else if(REMOTE_CONTROL){obrazovka=5;return;}
        else if(PROGRAM1)      {obrazovka=6;return;}
        else if(PROGRAM2)      {obrazovka=7;return;}
        else if(PROGRAM3)      {obrazovka=8;return;}
        else if(PROGRAM4)      {obrazovka=9;return;}
        else if(PROGRAM5)      {obrazovka=10;return;}
        else if(PROGRAM6)      {obrazovka=11;return;}
     }    
    }   
  }
  void Manual()
  {
    wait_ms(1000);
    lcd.Clear(LCD_COLOR_BLUE);
    Text(20, LINE(0),16,barva_textu_pozadi,barva_pozadi,"ROTACE");
    Text(109,LINE(0),16,barva_textu_pozadi,barva_pozadi,"ZVEDANI");
    Text(208,LINE(0),16,barva_textu_pozadi,barva_pozadi,"RAMENO");
    Text(302,LINE(0),16,barva_textu_pozadi,barva_pozadi,"KLESTE");
    Text(386,LINE(0),16,barva_textu_pozadi,barva_pozadi,"RYCHLOST");
    
    Button(10,18,84,85, barva_tlacitka,barva_textu_tlacitko);      Text(20,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"DOLEVA");
    Button(104,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(114,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"NAHORU");
    Button(198,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(207,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"NAHORU");
    Button(292,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(297,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"OTEVRIT");
    Button(386,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(420,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"+");
         
    Button(10,113,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(15,LINE(9),16,barva_textu_tlacitko, barva_tlacitka,"DOPRAVA");
    Button(104,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(114,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"DOLU"); 
    Button(198,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(207,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"DOLU");
    Button(292,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(302,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"ZAVRIT");
    Button(386,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(420,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"-");
  
     while(1)
     {    
        Precti_Gyro();
        VytiskniPozice(10,LINE(13),16,LCD_COLOR_WHITE,LCD_COLOR_BLUE);
        VytiskniGyro(10,LINE(14),16,LCD_COLOR_WHITE, LCD_COLOR_BLUE);
        if(NactiDotek()>0)
        {    
            if(M_RYCHLOST_PLUS)             {spd=spd+1000; if(spd>=999000) spd=999000;} // rychlost plus
            else if(M_RYCHLOST_MINUS)       {spd=spd-1000; if(spd<=0) spd=0;} // rychlost minus
            
            else if(M_ROTACE_DOLEVA)        {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);} // jede doleva 1
            else if(M_ROTACE_DOPRAVA)       {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);} // jede doprava 1
            
            else if(M_ZVEDANI_DOLU)         {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);} // jede dolu 2
            else if(M_ZVEDANI_NAHORU)       {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);} // jede nahoru 2
            
            else if(M_RAMENO_NAHORU)        {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);} // jede nahoru 4
            else if(M_RAMENO_DOLU)          {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);} // jede dolu 4
            
            else if(M_KLESTE_OTEVRIT)       {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);} // jede nahoru 4
            else if(M_KLESTE_ZAVRIT)        {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);} // jede dolu 4
    
            else if(M_POS1)                 {WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);}   // vynulovani pozic
            else if(M_POS2)                 {WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER2);}   // vynulovani pozic
            else if(M_POS3)                 {WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);}   // vynulovani pozic
            else if(M_POS4)                 {WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);}   // vynulovani pozic
            
            else if(x>386 && x<470 && y>198 && y<272)
            {
              obrazovka=1;return;
            }
          }
          
           else
           {
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
           }
         wait(0.1);
   } 
  }
 void Homing()
 {
     
     lcd.ClearStringLine(1);
     lcd.ClearStringLine(2);
     lcd.ClearStringLine(13);
     lcd.ClearStringLine(14);
     lcd.ClearStringLine(15);
     lcd.Clear(LCD_COLOR_BLUE);
     Text(150,91,24,LCD_COLOR_WHITE,LCD_COLOR_BLUE,"Nulovani os...");
     Text(150,120,24,LCD_COLOR_WHITE,LCD_COLOR_BLUE,"0/4");
     WriteDriverCommand(POWERSTEP01_RUN_REV, 20000, DRIVER3);  //kleste
     wait(0.5);
     ACC_X=30;
     while(ACC_X>0)
     {           
        Precti_Gyro();
        wait(0.1);
     }
     WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);  Text(150,120,24,LCD_COLOR_WHITE,LCD_COLOR_BLUE,"1/4");   
     WriteDriverCommand(POWERSTEP01_RUN_REV, 20000, DRIVER4);  // rameno
      
     wait(0.5);
     ACC_X=1000;
     while(ACC_X>-50)
     {           
        Precti_Gyro();
        wait(0.1);
     }
     WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);    Text(150,120,24,LCD_COLOR_WHITE,LCD_COLOR_BLUE,"2/4");
     WriteDriverCommand(POWERSTEP01_RUN_FWD, 20000, DRIVER2);  
     
     wait(0.5);
     Gyro_X=-8;
     while(Gyro_X<-5)
     {           
       Precti_Gyro();
       wait(0.1);
     }
     WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);     Text(150,120,24,LCD_COLOR_WHITE,LCD_COLOR_BLUE,"UZ TO SKORO JE");
     WriteDriverCommand(POWERSTEP01_RUN_REV, 20000, DRIVER1);  
     wait(0.5);
     Gyro_Z=20;
     while(Gyro_Z>5)
     {           
        Precti_Gyro();
        wait(0.1);
     }
     WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);    Text(150,120,24,LCD_COLOR_WHITE,LCD_COLOR_BLUE,"HOTOVO!!!          ");    wait(1);
     obrazovka=1;
}
void RemoteControl()
{
      
       while(1)
    {    
    
        if(BT.readable())     // BT module receives a signal
        {
            BTread[i] = BT.getc();  // read a character of the signal 
            if(BTread[0] == 244) i++;             
        }
        
        if(i==6)
        {   
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
            pc.printf("%i %i %i %i %i %i\n\r", BTread[0], BTread[1], BTread[2], BTread[3], BTread[4], BTread[5]);
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 100) {Homing();}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 10) {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 20) {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 30) {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 40) {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 50) {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 60) {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 70) {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 80) {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);}
            if(BTread[0] == 244 && BTread[1] == 9 && BTread[2] == 3 && BTread[3] == 233 && BTread[4] == 0)  {WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
                                                                                                             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
                                                                                                             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
                                                                                                             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);}
           
            i = 0; // null for the next BT command
        }    
    }
}
void Uceni()
{
  Homing();  
  step1=0;
  step2=0;
  step3=0;
  step4=0;
  krok=0;
  WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);
  WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER2);
  WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);
  WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);
  
  lcd.Clear(LCD_COLOR_BLUE);
    Text(20, LINE(0),16,barva_textu_pozadi,barva_pozadi,"ROTACE");
    Text(109,LINE(0),16,barva_textu_pozadi,barva_pozadi,"ZVEDANI");
    Text(208,LINE(0),16,barva_textu_pozadi,barva_pozadi,"RAMENO");
    Text(302,LINE(0),16,barva_textu_pozadi,barva_pozadi,"KLESTE");
    Text(386,LINE(0),16,barva_textu_pozadi,barva_pozadi,"RYCHLOST");
    
    Button(10,18,84,85, barva_tlacitka,barva_textu_tlacitko);      Text(20,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"DOLEVA");
    Button(104,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(114,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"NAHORU");
    Button(198,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(207,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"NAHORU");
    Button(292,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(297,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"OTEVRIT");
    Button(386,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(420,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"+");
         
    Button(10,113,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(15,LINE(9),16,barva_textu_tlacitko, barva_tlacitka,"DOPRAVA");
    Button(104,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(114,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"DOLU"); 
    Button(198,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(207,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"DOLU");
    Button(292,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(302,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"ZAVRIT");
    Button(386,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(420,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"-");
    Button(10,225,366,40,LCD_COLOR_GREEN,LCD_COLOR_WHITE);         Text(170,LINE(15),16,barva_textu_tlacitko,LCD_COLOR_GREEN,"SAVE");
    Button(386,225,84,40,LCD_COLOR_GREEN,LCD_COLOR_WHITE);         Text(405,LINE(15),16,barva_textu_tlacitko,LCD_COLOR_GREEN,"EXIT");
  WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
    for(int k=0;k<=100;k++)
     {
        pozice1[k]=0;  
        pozice2[k]=0;
        pozice3[k]=0;  
        pozice4[k]=0;  
     }
     while(1)
     {    
        Precti_Gyro();
        VytiskniPozice(10,LINE(13),16,LCD_COLOR_WHITE,LCD_COLOR_BLUE);

        if(NactiDotek()>0)
        {    
           
            if(M_RYCHLOST_PLUS)             {spd=spd+1000; if(spd>=999000) spd=999000;} // rychlost plus
            else if(M_RYCHLOST_MINUS)       {spd=spd-1000; if(spd<=0) spd=0;} // rychlost minus
            
            else if(M_ROTACE_DOLEVA)        {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);} // jede doleva 1
            else if(M_ROTACE_DOPRAVA)       {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);} // jede doprava 1
            
            else if(M_ZVEDANI_DOLU)         {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);} // jede dolu 2
            else if(M_ZVEDANI_NAHORU)       {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);} // jede nahoru 2
            
            else if(M_RAMENO_NAHORU)        {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);} // jede nahoru 4
            else if(M_RAMENO_DOLU)          {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);} // jede dolu 4
            
            else if(M_KLESTE_OTEVRIT)       {WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);} // jede nahoru 4
            else if(M_KLESTE_ZAVRIT)        {WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);} // jede dolu 4
    
            else if(SAVE)                    {
                                                        krok=krok+1;
                                                        pozice1[krok]=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
                                                        pozice2[krok]=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
                                                        pozice4[krok]=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
                                                        pozice3[krok]=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
                                             }   
      
            else if(EXIT)
            {
                Homing();
                lcd.Clear(LCD_COLOR_GREEN);
                Text(180,130,24,LCD_COLOR_BLACK,LCD_COLOR_GREEN,"WORKING ...");
                pozice1[0]=0;
                WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);
                POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
                WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);
                WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);
            for(int i=1;i<=krok;i++)
            {                   
                POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
                if(pozice1[i]<pozice1[i-1])
                {
                    while(POS1>=pozice1[i])
                    {
                      POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
                      WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);
                    }
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
                }         
                if(pozice1[i]>pozice1[i-1])
                {
                    while(POS1<=pozice1[i])
                    {
                      POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
                      WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);
                    }
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
                } 
                
                POS2=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
                if(pozice2[i]>pozice2[i-1] && i==1)
                {
                    while(POS2<=pozice2[i])
                    {
                      POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
                      WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
                    }
                    while(POS2>=pozice2[i])
                    {
                      POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
                      WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
                    }
                    
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
                }        
                if(pozice2[i]<pozice2[i-1])
                {
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
                }
                WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
                if(pozice2[i]>pozice2[i-1] && i!=1)
                {
                    while(POS2<=pozice2[i])
                    {
                      POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
                      WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);
                    }
                    
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
                }         
                WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
                
                POS3=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
                if(pozice3[i]<pozice3[i-1])
                {
                    while(POS3>=pozice3[i])
                    {
                      POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
                      WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);
                    }
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
                }         
                if(pozice3[i]>pozice3[i-1])
                {
                    while(POS3<=pozice3[i])
                    {
                      POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
                      WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);
                    }
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
                } 
                
                POS4=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
                if(pozice4[i]<pozice4[i-1])
                {
                    while(POS4>=pozice4[i])
                    {
                      POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
                      WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);
                    }
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
                }         
                
                if(pozice4[i]>pozice4[i-1])
                {
                    POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
                    while(POS4<=pozice4[i])
                    {
                      POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
                      WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);
                    }
                    WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
                }     
            }
            Ukladani();return;
            } 
           }         
           else
           {
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
             WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
           }
           wait(0.1);
   } 
}
void Ukladani()
{

    lcd.Clear(LCD_COLOR_BLUE);
    lcd.SetBackColor(LCD_COLOR_BLUE);
    ts.Init(lcd.GetXSize(), lcd.GetYSize()); 
    
   lcd.Clear(LCD_COLOR_BLUE);
    
    Text(140,LINE(0),16,barva_textu_pozadi,barva_pozadi,"CHOOSE THE PROGRAM: ");

    
    Button(10,18,84,85, LCD_COLOR_RED,LCD_COLOR_WHITE);            Text(20,LINE(3),16,LCD_COLOR_WHITE,LCD_COLOR_RED,"DELETE");
    Button(104,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(140,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"1");
    Button(198,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(230,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"2");
    Button(292,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(330,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"3");
    Button(386,18,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(420,LINE(3),16,barva_textu_tlacitko,barva_tlacitka,"4");
         
    Button(10,113,84,85,barva_tlacitka,barva_textu_tlacitko);      Text(55,LINE(9),16,barva_textu_tlacitko, barva_tlacitka,"5");
    Button(104,113,84,85,barva_tlacitka,barva_textu_tlacitko);     Text(139,LINE(9),16,barva_textu_tlacitko,barva_tlacitka,"6"); 
   
    
  while(1)
  {
   if(NactiDotek()>0)
   {
      if(M_RYCHLOST_PLUS)             {program4_step=krok;for(int f=1;f<=krok;f++){program4[1][f]=pozice1[f];program4[2][f]=pozice2[f];program4[3][f]=pozice3[f];program4[4][f]=pozice4[f];} obrazovka=1;return;} 
      else if(M_ROTACE_DOLEVA)        {program5_step=krok;for(int f=1;f<=krok;f++){program5[1][f]=pozice1[f];program5[2][f]=pozice2[f];program5[3][f]=pozice3[f];program5[4][f]=pozice4[f];} obrazovka=1;return;} 
            
      else if(M_ROTACE_DOPRAVA)       {obrazovka=1;return;} 
      else if(M_ZVEDANI_NAHORU)       {program1_step=krok;for(int f=1;f<=krok;f++){program1[1][f]=pozice1[f];program1[2][f]=pozice2[f];program1[3][f]=pozice3[f];program1[4][f]=pozice4[f];} obrazovka=1;return;} 
      else if(M_RAMENO_NAHORU)        {program2_step=krok;for(int f=1;f<=krok;f++){program2[1][f]=pozice1[f];program2[2][f]=pozice2[f];program2[3][f]=pozice3[f];program2[4][f]=pozice4[f];} obrazovka=1;return;} 
      else if(M_KLESTE_OTEVRIT)       {program3_step=krok;for(int f=1;f<=krok;f++){program3[1][f]=pozice1[f];program3[2][f]=pozice2[f];program3[3][f]=pozice3[f];program3[4][f]=pozice4[f];} obrazovka=1;return;}
      
      else if(M_ZVEDANI_DOLU)         {program6_step=krok;for(int f=1;f<=krok;f++){program6[1][f]=pozice1[f];program6[2][f]=pozice2[f];program6[3][f]=pozice3[f];program6[4][f]=pozice4[f];} obrazovka=1;return;} 
      
     
   }   
  }
}
void Program1()
{
    Homing();
    lcd.Clear(LCD_COLOR_GREEN);
    Text(180,130,24,LCD_COLOR_BLACK,LCD_COLOR_GREEN,"WORKING ...");
    pozice1[0]=0;
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);
    POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);
    for(int i=1;i<=program1_step;i++)
    {   
        POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
        if(program1[1][i]<program1[1][i-1])
        {
            while(POS1>=program1[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        }         
        if(program1[1][i]>program1[1][i-1])
        {
            while(POS1<=program1[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        } 
        
        POS2=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
        if(program1[2][i]>program1[2][i-1] && i==1)
        {
            while(POS2<=program1[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            while(POS2>=program1[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }        
        if(program1[2][i]<program1[2][i])
        {
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        if(program1[2][i]>program1[2][i-1] && i!=1)
        {
            while(POS2<=program1[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }         
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        
        POS3=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
        if(program1[3][i]<program1[3][i-1])
        {
            while(POS3>=program1[3][i])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        }         
        if(program1[3][i]>program1[3][i-1])
        {
            while(POS3<=program1[3][i-1])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        } 
        
        POS4=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
        if(program1[4][i]<program1[4][i-1])
        {
            while(POS4>=program1[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }         
        
        if(program1[4][i]>program1[4][i-1])
        {
            POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
            while(POS4<=program1[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }     
    }
    UvodniObrazovka();return;
    
}
void Program2()
{
    Homing();
    lcd.Clear(LCD_COLOR_GREEN);
    Text(180,130,24,LCD_COLOR_BLACK,LCD_COLOR_GREEN,"WORKING ...");
    pozice1[0]=0;
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);
    POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);
    for(int i=1;i<=program2_step;i++)
    {   
        POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
        if(program2[1][i]<program2[1][i-1])
        {
            while(POS1>=program2[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        }         
        if(program2[1][i]>program2[1][i-1])
        {
            while(POS1<=program2[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        } 
        
        POS2=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
        if(program2[2][i]>program2[2][i-1] && i==1)
        {
            while(POS2<=program2[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            while(POS2>=program2[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }        
        if(program2[2][i]<program2[2][i])
        {
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        if(program2[2][i]>program2[2][i-1] && i!=1)
        {
            while(POS2<=program2[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }         
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        
        POS3=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
        if(program2[3][i]<program2[3][i-1])
        {
            while(POS3>=program2[3][i])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        }         
        if(program2[3][i]>program2[3][i-1])
        {
            while(POS3<=program2[3][i-1])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        } 
        
        POS4=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
        if(program2[4][i]<program2[4][i-1])
        {
            while(POS4>=program2[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }         
        
        if(program2[4][i]>program2[4][i-1])
        {
            POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
            while(POS4<=program2[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }     
    }
    UvodniObrazovka();return;
}

void Program3()
{
    Homing();
    lcd.Clear(LCD_COLOR_GREEN);
    Text(180,130,24,LCD_COLOR_BLACK,LCD_COLOR_GREEN,"WORKING ...");
    pozice1[0]=0;
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);
    POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);
    for(int i=1;i<=program3_step;i++)
    {   
        POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
        if(program3[1][i]<program3[1][i-1])
        {
            while(POS1>=program3[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        }         
        if(program3[1][i]>program3[1][i-1])
        {
            while(POS1<=program3[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        } 
        
        POS2=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
        if(program3[2][i]>program3[2][i-1] && i==1)
        {
            while(POS2<=program3[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            while(POS2>=program3[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }        
        if(program3[2][i]<program3[2][i])
        {
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        if(program3[2][i]>program3[2][i-1] && i!=1)
        {
            while(POS2<=program3[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }         
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        
        POS3=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
        if(program3[3][i]<program3[3][i-1])
        {
            while(POS3>=program3[3][i])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        }         
        if(program3[3][i]>program3[3][i-1])
        {
            while(POS3<=program3[3][i-1])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        } 
        
        POS4=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
        if(program3[4][i]<program3[4][i-1])
        {
            while(POS4>=program3[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }         
        
        if(program3[4][i]>program3[4][i-1])
        {
            POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
            while(POS4<=program3[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }     
    }
    UvodniObrazovka();return;
}
void Program4()
{
    Homing();
    lcd.Clear(LCD_COLOR_GREEN);
    Text(180,130,24,LCD_COLOR_BLACK,LCD_COLOR_GREEN,"WORKING ...");
    pozice1[0]=0;
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);
    POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);
    for(int i=1;i<=program4_step;i++)
    {   
        POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
        if(program4[1][i]<program4[1][i-1])
        {
            while(POS1>=program4[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        }         
        if(program4[1][i]>program4[1][i-1])
        {
            while(POS1<=program4[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        } 
        
        POS2=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
        if(program4[2][i]>program4[2][i-1] && i==1)
        {
            while(POS2<=program4[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            while(POS2>=program4[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }        
        if(program4[2][i]<program4[2][i])
        {
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        if(program4[2][i]>program4[2][i-1] && i!=1)
        {
            while(POS2<=program4[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }         
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        
        POS3=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
        if(program4[3][i]<program4[3][i-1])
        {
            while(POS3>=program4[3][i])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        }         
        if(program4[3][i]>program4[3][i-1])
        {
            while(POS3<=program4[3][i-1])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        } 
        
        POS4=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
        if(program4[4][i]<program4[4][i-1])
        {
            while(POS4>=program4[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }         
        
        if(program4[4][i]>program4[4][i-1])
        {
            POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
            while(POS4<=program4[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }     
    }
    UvodniObrazovka();return;
}
void Program5()
{
    Homing();
    lcd.Clear(LCD_COLOR_GREEN);
    Text(180,130,24,LCD_COLOR_BLACK,LCD_COLOR_GREEN,"WORKING ...");
    pozice1[0]=0;
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);
    POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);
    for(int i=1;i<=program5_step;i++)
    {   
        POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
        if(program5[1][i]<program5[1][i-1])
        {
            while(POS1>=program5[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        }         
        if(program5[1][i]>program5[1][i-1])
        {
            while(POS1<=program5[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        } 
        
        POS2=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
        if(program5[2][i]>program5[2][i-1] && i==1)
        {
            while(POS2<=program5[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            while(POS2>=program5[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }        
        if(program5[2][i]<program5[2][i])
        {
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        if(program5[2][i]>program5[2][i-1] && i!=1)
        {
            while(POS2<=program5[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }         
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        
        POS3=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
        if(program5[3][i]<program5[3][i-1])
        {
            while(POS3>=program5[3][i])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        }         
        if(program5[3][i]>program5[3][i-1])
        {
            while(POS3<=program5[3][i-1])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        } 
        
        POS4=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
        if(program5[4][i]<program5[4][i-1])
        {
            while(POS4>=program5[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }         
        
        if(program5[4][i]>program5[4][i-1])
        {
            POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
            while(POS4<=program5[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }     
    }
    UvodniObrazovka();return;
}
void Program6()
{
    Homing();
    lcd.Clear(LCD_COLOR_GREEN);
    Text(180,130,24,LCD_COLOR_BLACK,LCD_COLOR_GREEN,"WORKING ...");
    pozice1[0]=0;
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER1);
    POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER4);
    WriteDriverCommand(POWERSTEP01_ABS_POS, 0, DRIVER3);
    for(int i=1;i<=program6_step;i++)
    {   
        POS1=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
        if(program6[1][i]<program6[1][i-1])
        {
            while(POS1>=program6[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        }         
        if(program6[1][i]>program6[1][i-1])
        {
            while(POS1<=program6[1][i])
            {
              POS1= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER1);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER1);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER1);
        } 
        
        POS2=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
        if(program6[2][i]>program6[2][i-1] && i==1)
        {
            while(POS2<=program6[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            while(POS2>=program6[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }        
        if(program6[2][i]<program6[2][i])
        {
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        if(program6[2][i]>program6[2][i-1] && i!=1)
        {
            while(POS2<=program6[2][i])
            {
              POS2= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER2);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER2);
            }
            
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        }         
        WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER2);
        
        POS3=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
        if(program6[3][i]<program6[3][i-1])
        {
            while(POS3>=program6[3][i])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        }         
        if(program6[3][i]>program6[3][i-1])
        {
            while(POS3<=program6[3][i-1])
            {
              POS3= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER3);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER3);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER3);
        } 
        
        POS4=ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
        if(program6[4][i]<program6[4][i-1])
        {
            while(POS4>=program6[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_REV, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }         
        
        if(program6[4][i]>program6[4][i-1])
        {
            POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
            while(POS4<=program6[4][i])
            {
              POS4= ReadDriverRegister(POWERSTEP01_ABS_POS, DRIVER4);
              WriteDriverCommand(POWERSTEP01_RUN_FWD, spd, DRIVER4);
            }
            WriteDriverCommand(POWERSTEP01_HARD_HIZ, 0, DRIVER4);
        }     
    }
    UvodniObrazovka();return;
}